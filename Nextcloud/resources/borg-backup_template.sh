#!/bin/bash

set -e
cleanup()
{
    echo "Something bad happened during backup, check ${LOG_PATH}"
    exit $1
}
ts_log()
{
    echo `date '+%Y-%m-%d_%H:%M:%S'`" - "$1 >> /var/log/borg-${CLEANDOMAIN}.log
}
trap '[ "$?" -eq 0 ] || cleanup' EXIT   # Trap on non-zero exit

ts_log "Starting new backup ${BACKUP_DATE}..."

BACKUP_DATE=`date +%Y%m%d_%H%M%S`
LOG_PATH=/var/log/borg-${CLEANDOMAIN}.log

BORG=/usr/bin/borg
export BORG_RSH="ssh -i /root/.ssh/${CLEANDOMAIN}"
export BORG_PASSPHRASE="`cat /root/.borg/${CLEANDOMAIN}`"
BORG_REPOSITORY=${CLEANDOMAIN}:/data/${CLEANDOMAIN}/borg/${SERVERNAME}
BORG_ARCHIVE=${BORG_REPOSITORY}::${BACKUP_DATE}

ts_log 'Dumping MySQL db...'
MYSQL_PASS=`cat /root/.borg/mysql_${CLEANDOMAIN}`
MYSQL_USER="${CLEANDOMAIN}"
MYSQL_DATABASE="nc_${CLEANDOMAIN}"
MYSQL_TMP_DUMP_FILE=/data/maintenance/sauvegardes/mysql_${CLEANDOMAIN}_db.sql

mysqldump -u$MYSQL_USER --events -p$MYSQL_PASS $MYSQL_DATABASE > $MYSQL_TMP_DUMP_FILE

ts_log "Pushing archive ${BORG_ARCHIVE}"
$BORG create \
     -v --stats --compression lzma,9 \
     $BORG_ARCHIVE \
     /data/cloud.${DOMAIN} $MYSQL_TMP_DUMP_FILE \
     --exclude '/home/*/.ssh' \
     --exclude '/home/*/cache' \
     --exclude '/root/.borg' \
     --exclude '/root/.ssh' \
     >> ${LOG_PATH} 2>&1

ts_log "Rotating old backups."
$BORG prune -v $BORG_REPOSITORY \
      --keep-daily=31 \
      --keep-weekly=4 \
      --keep-monthly=2 \
      >> ${LOG_PATH} 2>&1

ts_log 'Cleaning up...'
rm $MYSQL_TMP_DUMP_FILE

ts_log 'Archive listing...'
$BORG list $BORG_REPOSITORY