#!/bin/bash

filesystems[0]="user1/files"
filesystems[1]="user2/files" 
cd "/nextcloud/data/folder/"

emAdy="email@example.com"
emSub="Warning: Nextcloud Contains 0 Byte Files"

for fs in "${filesystems[@]}"; do

        while read -d '' -r; do
    		arr+=( "$REPLY\n" )
		done < <(find $fs -size 0 -print0)

done

if [[ ! -z ${arr[@]} ]]; then

	echo -e ${arr[@]} | mail -s "$emSub" "$emAdy"

fi
