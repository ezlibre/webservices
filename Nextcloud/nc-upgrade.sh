#!/bin/bash

## Upgrade Nextcloud
#
# version 2.2.1 - 31/07/2024
#
# This script upgrade your Nextcloud installation following these steps:
# - ~~check presence of dependencies (needed commands)~~: to simplify this script an stop grow it,
#                                                         we consider webserver is functional
# - check if latest version is already installed
# - download latest version and check integrity of the file with GPG
# - backup current installation
# - apply upgrade configuration, data and applications
# - adjust permissions
#
# Important:
# - It's to run under root or user that can modify files rights and use www-data
# - Intergity check is temporary disabled because of aws 3rd party module...
# - this script in version 2.1.x and more considers upgrading from Nextcloud 19 minimum on Debian 10 minimum
# - forget `restore` function that never clearly worked
#
# Usage: `./nc-upgrade.sh [-d|--dir {INSTALL_PATH}] \
#                         [-s|--save {SAVE_PATH}] \
#                         [-v|--version {VERSION}] \
#                         [-l|--log]`
## Examples:
# `./nc-upgrade.sh -d INST_PATH` will get latest version number from GitHub repo and ask if keeping save
# `./nc-upgrade.sh -d INST_PATH -s SAVE_PATH -v 21.0.4 -l`
# `./nc-upgrade.sh -d /var/www/nextcloud -s /home/www -l -v 21.0.4 >> /var/log/nc-upgrade.log 2>&1`
##

function main() {
    echo -e '\033[1m#Starting nextcloud upgrade\033[0m'
    echo "-- $(date +%F-%H-%M-%S) --"
    echo "-----------------------------------"

    # Reading vars
    readvars "$@"

    # Moving to working directory (SAVE)
    manage_permissions "start"

    # dependencies
    DEPS=( curl wget grep gawk sed gpg tar sudo rsync )
    check_dependencies

    # get enabled 3rdparty apps name
    test "${LOG}" = 'true' && echo -n "[LOG] Checking APPS... "
    APPS=$(
        sudo -u www-data php "${DIR}"/occ app:list --shipped=false | \
        grep '^  - ' | sed -r 's/ +- (.+)/\1/' | sed -r 's/(.+):.+/\1/'
    )
    test "${LOG}" = 'true' && echo "APPS=${APPS}"

    # get database information from nextcloud configuration
    DBTYPE=$(php -r "require(\"${DIR}/config/config.php\"); echo \$CONFIG[\"dbtype\"];" "${DIR}")
    DBNAME=$(php -r "require(\"${DIR}/config/config.php\"); echo \$CONFIG[\"dbname\"];" "${DIR}")
    DBHOST=$(php -r "require(\"${DIR}/config/config.php\"); echo \$CONFIG[\"dbhost\"];" "${DIR}")
    DBUSER=$(php -r "require(\"${DIR}/config/config.php\"); echo \$CONFIG[\"dbuser\"];" "${DIR}")
    DBPASS=$(php -r "require(\"${DIR}/config/config.php\"); echo \$CONFIG[\"dbpassword\"];" "${DIR}")
    CLIURL=$(php -r "require(\"${DIR}/config/config.php\"); echo \$CONFIG[\"overwrite.cli.url\"];" "${DIR}")
    test "${LOG}" = 'true' && echo "[LOG] DBNAME=${DBNAME}; DBHOST=${DBHOST}; DBUSER=${DBUSER}"

    # if empty, get latest version number from arg or from GitHub
    if [ -z "${LATEST_VERSION}" ]; then
        test "${LOG}" = 'true' && echo -n "[LOG] Fetching LATEST_VERSION... "
        LATEST_VERSION=$(
            curl -s https://github.com/nextcloud/server/releases.atom | \
            sed -n '/<title>v/{p;q}' | \
            sed -r 's/.+>v(.+)<.+/\1/'
        )
        test "${LOG}" = 'true' && echo "LATEST_VERSION=${LATEST_VERSION}"
    fi
    # get product and installed version number
    test "${LOG}" = 'true' && echo -n "[LOG] Getting infos on installed instance... "
    CURRENT_VERSION=$(sudo -u www-data php "${DIR}"/occ -V)
    CURRENT_VERSION=${CURRENT_VERSION##* } #only keep last part of result: version number
    PRODUCT=$(echo "${CLIURL}" | sed 's,https*://,,g')
    PRODUCT=$(echo "${PRODUCT%%/}" | sed 's,/,-,g')
    test "${LOG}" = 'true' && echo "${PRODUCT} ${CURRENT_VERSION} ; upgrade to nextcloud ${LATEST_VERSION}"

    echo -e '\033[1m#-- check version\033[0m'

    if [ "${CURRENT_VERSION}" == "${LATEST_VERSION}" ]; then
        echo '[WARNING] Already latest version'
        exit 1
    fi
	if [ "$((${CURRENT_VERSION%%.*} + 1))" -lt "${LATEST_VERSION%%.*}" ]; then
		echo '[ERROR] Upgrade between multiple major versions is unsupported (ex: 13 to 15)'
		exit 1
	fi

    if ! download; then
        echo '[ERROR] Error while downloading'
        exit 1
    fi
    if ! backup; then
        echo '[ERROR] Error while backuping'
        exit 1
    fi
    if ! upgrading; then
        echo '[ERROR] Error while upgrading'
        exit 1
    fi
    clean

    # Restore folders permissions
    manage_permissions "end"
}

function readvars() {
    echo -e '\033[1m#-- read vars\033[0m'

    KEEP_BACKUP="true"
    SAVE="$(pwd)"
    SAVE_ROOT="${SAVE}"
    LOG='false'

    while [[ $# -gt 0 ]]
    do
    key="$1"

    case $key in
        -d|--dir)
            if [ -w "$2" ]; then
                # ${VAR//ab/yz} replace all sub-chains 'ab' from VAR by 'yz'
                DIR="${2}/"
                DIR="${DIR//'//'//}"
            else
                echo "[ERROR] Invalid install dir : $2"
                exit 1
            fi
            shift
            ;;
        -s|--save)
            if [ -w "$2" ]; then
                SAVE="${2}/"
                SAVE="${SAVE//'//'//}"
                SAVE_ROOT="${SAVE}"
            else
                echo "[WARNING] Invalid save dir : $2"
                echo -n "> Save in current directory (y|o, default) or no save (n) ? "
                read answer
                if [ "$answer" = "n" ] || [ "$answer" = "N" ]; then
                    KEEP_BACKUP='false'
                fi
            fi
            shift
            ;;
        -v|--version)
            LATEST_VERSION="$2"
            shift
            ;;
        -l|--log)
            LOG='true'
            ;;
        *)
            echo "[WARNING] Unkown option: $1"
            ;;
    esac
    shift
    done

    test "${LOG}" = 'true' && echo "[LOG] DIR=${DIR}; SAVE=${SAVE}; VERSION=${LATEST_VERSION}; KEEP_BACKUP=${KEEP_BACKUP}"

	if [ -z "${DIR}" ]; then
		echo '[ERROR] No valid Nextcloud www path.'
		exit 1
	fi
    return 0
}

function download() {
    echo -e '\033[1m#-- download\033[0m'

    # Building URLs
    FILENAME="nextcloud-${LATEST_VERSION}.zip"
	DOWNLOAD_URL="https://download.nextcloud.com/server/releases/${FILENAME}"
	GPG_PUBKEY_URL="https://nextcloud.com/nextcloud.asc"
	#~ GPG_FINGERPRINT="A724937A"
    GPG_SIG_URL="${DOWNLOAD_URL}.asc"
    test "${LOG}" = 'true' && echo "[LOG] DOWNLOAD_URL=${DOWNLOAD_URL}"
    test "${LOG}" = 'true' && echo "[LOG] GPG_PUBKEY_URL=${GPG_PUBKEY_URL}"

    # download nextcloud and its signature file
    if [ ! -e "${FILENAME}" ]; then
        test "${LOG}" = 'true' && echo "[LOG] Downloading version and signature..."
        wget -q --show-progress "${DOWNLOAD_URL}"
        wget -q --show-progress "${GPG_SIG_URL}"
        wget -q --show-progress "${GPG_PUBKEY_URL}" -O - | gpg -q --import -
    fi

    # check integrity of the downloaded file
    test "${LOG}" = 'true' && echo "[LOG] Checking integrity..."
    gpg -q --verify "${FILENAME}".asc "$FILENAME" 2> /dev/null
    if [ $? != 0 ]; then
        echo '[ERROR] Mismatch between file and its signature'
        exit 1
    fi

    return 0
}

function backup() {
    echo -e '\033[1m#-- backup\033[0m'

    SAVE="${SAVE}${PRODUCT}-${CURRENT_VERSION}_$(date +%F-%H-%M-%S)"
    test "${LOG}" = 'true' && echo "[LOG] SAVE=${SAVE}"

    # enable mode maintenance
    test "${LOG}" = 'true' && echo "[LOG] Maintenance mode on..."
    sudo -u www-data php "${DIR}"/occ maintenance:mode --on

    # backup nextcloud directory and database except data dir
    test "${LOG}" = 'true' && echo -n "[LOG] Dumping directory and database... "
    cp -a "${DIR}" "${SAVE}"
    if [ "${DBTYPE}" = "mysql" ]; then
        mysqldump --no-tablespaces --add-drop-table --create-options --compress --max_allowed_packet=512M \
            -h "${DBHOST}" -u "${DBUSER}" -p"${DBPASS}" -B "${DBNAME}" > "${SAVE}".mysql 2> /dev/null
    elif [ "${DBTYPE}" = "pgsql" ]; then
        sudo -u postgres pg_dump -c --if-exists -Fc "${DBNAME}" > "${SAVE}".pgsql 2> /dev/null
    fi
    test "${LOG}" = 'true' && echo "dbdump="$?

    return 0
}

function upgrading() {
    echo -e '\033[1m#-- upgrade\033[0m'

    # extract downloaded archive and rename it
    test "${LOG}" = 'true' && echo "[LOG] Extract archive..."
    unzip -q "${FILENAME}"
    mv nextcloud "${PRODUCT}"

    # restore configuration
    test "${LOG}" = 'true' && echo "[LOG] Copy config.php to new dir..."
    cp -a "${SAVE}"/config/config.php "${PRODUCT}/config/"

    # restore apps if it is not bundled in package
    test "${LOG}" = 'true' && echo "[LOG] Copy Apps to new dir..."
    for app in ${APPS}; do
        test "${LOG}" = 'true' && echo -n "     - ${app}... "
        if [ ! -d "${SAVE_ROOT}/${PRODUCT}/apps/${app}" ]; then
            cp -a "${SAVE}"/apps/"${app}" "${PRODUCT}/apps/"
            test "${LOG}" = 'true' && echo $?" - copied"
        else
            test "${LOG}" = 'true' && echo "not copied (bundled in package)"
        fi
    done

    # replace installed nextcloud by the updated one
    test "${LOG}" = 'true' && echo "[LOG] Replace by new version..."
    rm -r "${DIR}"
    mv "${PRODUCT}" "${DIR}"

    # Disabling APCU
    if [ "${LATEST_VERSION%%.*}" -ge 21 ]; then
        test "${LOG}" = 'true' && echo "[LOG] NC21+ Disabling APCU..."
        sed -i '/.*memcache.*[aA][pP][cC][uU].*/d' "${DIR}"/config/config.php
    fi

    # Adjust folders permissions
    manage_permissions "upgrade"

    # disable mode maintenance
    test "${LOG}" = 'true' && echo "[LOG] Maintenance mode off..."
    sudo -u www-data php "${DIR}"/occ maintenance:mode --off

    # launch nextcloud upgrade process
    test "${LOG}" = 'true' && echo "[LOG] Start occ upgrade..."
	sudo -u www-data php "${DIR}"/occ upgrade

    # update .htaccess to handle pretty url
    test "${LOG}" = 'true' && echo "[LOG] Update .htaccess..."
    CLIINDEX=$(echo "${CLIURL}" | awk -F"/" '{print length($0)-length($NF)}')	# calculate last / index in string
	if [ "${#CLIURL}" != "${CLIINDEX}" ]; then									# if no ending / then adding it
		sed -i "s/\(overwrite.cli.url.*\)',/\1\/',/g" "${DIR}"/config/config.php
    fi
    sudo -u www-data php "${DIR}"/occ maintenance:update:htaccess

    # database operations according to Nextcloud version
    test "${LOG}" = 'true' && echo "[LOG] NC13+ Database operations..."
    sudo -u www-data php "${DIR}"/occ db:add-missing-indices
    echo -e "y" | sudo -u www-data php "${DIR}"/occ db:convert-filecache-bigint
    test "${LOG}" = 'true' && echo "[LOG] NC19+ Database operations..."
    sudo -u www-data php "${DIR}"/occ db:add-missing-columns
    if [ "${LATEST_VERSION%%.*}" -ge 20 ]; then
        test "${LOG}" = 'true' && echo "[LOG] NC20+ Database operations..."
        sudo -u www-data php "${DIR}"/occ db:add-missing-primary-keys
    fi
    if [ "${LATEST_VERSION%%.*}" -ge 21 ]; then
        test "${LOG}" = 'true' && echo "[LOG] NC21+ Config file parameter..."
        grep -q 'default_phone_region' "${DIR}"/config/config.php || sed -i "s/);/\  'default_phone_region' => 'FR',\n);/g" "${DIR}"/config/config.php
    fi
    if [ "${LATEST_VERSION%%.*}" -ge 28 ]; then
        test "${LOG}" = 'true' && echo "[LOG] NC28+ Config file parameter..."
        grep -q 'maintenance_window_start' "${DIR}"/config/config.php || sed -i "s/);/\  'maintenance_window_start' => '1',\n);/g" "${DIR}"/config/config.php
        sudo -u www-data php "${DIR}"/occ maintenance:repair --include-expensive
    fi

    # check state of the installation after upgrade
    test "${LOG}" = 'true' && echo -n "[LOG] Checking state and integrity post install... "
    sed -i '/.*integrity\.check\.disabled.*/d' "${DIR}"/config/config.php
    STATE=$(sudo -u www-data php "${DIR}"/occ status | grep 'installed' | sed -r 's/.+installed: (.+)/\1/')
    INTEGRITY=$(
        sudo -u www-data php "${DIR}"/occ integrity:check-core | \
        grep 'INVALID_HASH' | \
        sed 's/[-|\s|:]//g'
    )
    test "${LOG}" = 'true' && echo "STATE=${STATE}; INTEGRITY=${INTEGRITY}"

    return 0
}

function check_dependencies() {
    echo -e '\033[1m#-- check dependencies\033[0m'

    for dep in "${DEPS[@]}"; do
        if [[ "$(dpkg-query -W --showformat='${Status}\n' ${dep})" == "install ok installed" ]]; then
            DEPS=( "${DEPS[@]/$dep}" )
            DEPS="${DEPS//  / }"
        fi
    done
    if (( ${#DEPS[@]} > 0 )); then
        echo "[LOG] Trying to install missing dependencies..."
        apt install -y "${DEPS[@]}" || { echo "[ERROR] Dependency install error."; exit 1; }
    fi

    return 0
}

function manage_permissions() {
    if [[ "$1" == "start" ]]; then
        test "${LOG}" = 'true' && echo "[LOG] Moving to 'save' directory..."
        cd "${SAVE}" || exit 1
    elif [[ "$1" == "upgrade" ]]; then
        # Update permissions to be sure there is no problem with the script
        test "${LOG}" = 'true' && echo "[LOG] Updating permissions..."
        chown -R www-data: "${DIR}"
    elif [[ "$1" == "end" ]]; then
        # Restore permissions
        test "${LOG}" = 'true' && echo "[LOG] Restoring permissions..."
        chmod -R o-rwx "${DIR}"
        chown -R root:www-data "${DIR}"
        chown -R www-data: "${DIR}config"
        chown -R www-data: "${DIR}apps"
		if [ -d "${DIR}data" ]; then
			chown -R www-data: "${DIR}data"
		fi
    fi

    return 0
}

function clean() {
    echo -e '\033[1m#-- clear\033[0m'

    if [ "${KEEP_BACKUP}" != 'true' ]; then
        test "${LOG}" = 'true' && echo "[LOG] Removing directory and SQL backup..."
        rm -rf "$SAVE"
        rm -f "$SAVE".*sql
    fi

    return 0
}

if [[ "${BASH_SOURCE}" == "$0" ]]; then
    main "$@"
fi
