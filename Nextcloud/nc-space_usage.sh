#!/bin/bash
# Version 2.1.3 - 20 sept. 2021
set -E

trap 'catch $?' ERR

catch() {
        if [ "$1" == 0 ]; then
                return
        fi
        LOGS+="\nCaught error $1 in "
        frame=0
        line="$(caller $frame 2>&1 | cut -d ' ' -f 1)"
        while [ -n "$line" ]; do
                subroutine="$(caller $frame 2>&1 | cut -d ' ' -f 2)"
                file="$(caller $frame 2>&1 | cut -d ' ' -f 3)"
                LOGS+="$file:$line in $subroutine\n"
                LOGS+="    $(sed -n ${line}p $file)\n\n"
                ((frame++)) || true
                line="$(caller $frame 2>&1 | cut -d ' ' -f 1)"
        done
        LOGS+="Context: ${CONTEXT}"
        log "${LOGS}"
        echo -e "${LOGS}" | mail -a "Content-Type: text/plain; charset=UTF-8" \
                -s "[Quotas] Script error" "${MAILS:-"support@lydra.fr"}"
        exit "$1"
}

CURRENT_DATE=$(date +%d/%m/%Y_%Hh%M)

usage() {
        echo "$(basename ${0}) -p[ath] <WEBROOTPATH> [ OPTIONS ]"
        echo ""
        echo "OPTIONS = { -h[elp] | -d[ebug] }"
        echo "OPTIONS = { -m[ails] <EMAIL1,EMAIL2> } : email list (comma separated) to *systematically* send report."
        echo "                                         If not defined, only over-quota will be mailed to default mail."
        echo "OPTIONS = { -p[ath] <WEBROOTPATH> } : Nextcloud web root path."
        echo "OPTIONS = { -q[uota] <space in Go>_<number of users> } : Quota from subscription."
        echo "OPTIONS = { -e[xpand] } : Print details user by user."
        echo "---"
        echo ""
}

log() {
        if [ -n "${DEBUG}" ]; then
                echo -e "$*"
        fi
}

init() {
        readonly  ERR="[\033[1;31mERROR\033[0m] "
        readonly   OK="[\033[1;32mOK\033[0m]    "
        readonly WARN="[\033[1;33mWARN\033[0m]  "
        readonly INFO="[\033[36mINFO\033[0m]  "
}

parameters() {
        CONTEXT="$*"
        while [[ $# -gt 0 ]]; do
                local key="$1"
                case $key in
                        -d|-debug)
                                DEBUG='true'
                                log "${INFO}Debug logs activated"
                                ;;
                        -e|-expand)
                                EXPAND='true'
                                ;;
                        -h|--help)
                                usage
                                exit 0
                                ;;
                        -m|-mails)
                                MAILS="${2}"
                                shift
                                ;;
                        -p|-path)
                                WEBROOTPATH="${2}"
                                shift
                                ;;
                        -q|-quota)
                                QUOTA="${2}"
                                shift
                                ;;
                        *)
                                log "${WARN}Unkown option: $1"
                                ;;
                esac
                shift
        done

        if [[ ! -d "${WEBROOTPATH}" ]]; then
                LOGS="Missing or wrong path info: ${WEBROOTPATH}."
                log "${ERR}${LOGS}"
                usage
                return 1
        fi
        if [[ -n "${MAILS}" ]] && [[ ! "${MAILS}" =~ ^([,]?[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})+$ ]]; then
                LOGS="Invalid mail(s): ${MAILS}."
                log "${WARN}${LOGS}"
        elif [[ -z "${MAILS}" ]]; then
                log "${INFO}No mail defined."
        fi
        if [[ -n "${QUOTA}" ]] && [[ ! "${QUOTA}" =~ ^[0-9]+_[0-9]+$ ]]; then
                LOGS="Invalid quota format: ${QUOTA}. Use <space in Go>_<number of users>."
                log "${WARN}${LOGS}"
                unset QUOTA
        else
                SPACE_QUOTA=$(echo "${QUOTA}" | cut -f1 -d'_')
                USERS_QUOTA=$(echo "${QUOTA}" | cut -f2 -d'_')
        fi
}

init
parameters "$@"
log "\n---------------------"
log "${INFO}Starting space usage - ${CURRENT_DATE}\n"
log "${INFO}VARS: SPACE_QUOTA=${SPACE_QUOTA} ; USERS_QUOTA=${USERS_QUOTA} ; MAILS=${MAILS}"

WEBROOTPATH="${WEBROOTPATH%/}"
DATAPATH=$(php -r "require(\"${WEBROOTPATH}/config/config.php\"); echo \$CONFIG[\"datadirectory\"];" "${WEBROOTPATH}")
DOMAIN=$(php -r "require(\"${WEBROOTPATH}/config/config.php\"); echo \$CONFIG[\"overwrite.cli.url\"];" "${WEBROOTPATH}")
DOMAIN=$(echo "${DOMAIN}" | sed -r 's%^.*//(.*)[/?]$%\1%')
log "${INFO}VARS: WEBROOTPATH=${WEBROOTPATH} ; DATAPATH=${DATAPATH} ; DOMAIN=${DOMAIN}"

GLOBAL=$(du -sh "${DATAPATH}" | sed "s/${DATAPATH//\//\\\/}//g")
QUOTA=$(du -sh -m --exclude="*/files_trashbin*" --exclude="*/trash*" --exclude="*/cache*" --exclude="*/appdata*" \
        --exclude="*/files_external*" --exclude="*/files_versions*" --exclude="*/versions*" --exclude="*/thumbnails*" \
        --exclude="*/updater_backup*" --exclude="*/uploads*" --exclude="*/locks*" --exclude="*/onlyoffice*" "${DATAPATH}" | \
        sed "s/${DATAPATH//\//\\\/}//g")
QUOTA=$(( QUOTA / 1024 ))
if [ -n "${EXPAND}" ]; then
        EXPAND=$(du -d 1 --exclude="*appdata*" --exclude="*files_external*" -h "${DATAPATH}" | /bin/sed "s/${DATAPATH//\//\\\/}//g")
fi
log "${INFO}VARS: GLOBAL=${GLOBAL} ; QUOTA=${QUOTA}G"

TOTALUSERS=$(sudo -u www-data php "${WEBROOTPATH%/}"/occ user:report | grep 'total' | sed -r 's/^[^0-9]*([0-9]+).*/\1/')
DISABLEDUSERS=$(sudo -u www-data php "${WEBROOTPATH%/}"/occ user:report | grep 'disabled' | sed -r 's/^[^0-9]*([0-9]+).*/\1/')
USERS=$((TOTALUSERS - DISABLEDUSERS - 1))
log "${INFO}VARS: USERS=${USERS} (Total=${TOTALUSERS} ; Disabled=${DISABLEDUSERS})"

PRINT="Statistiques pour ${DOMAIN} - ${CURRENT_DATE}\n\n \
        Espace disque :\n \
        - Comptabilisation Nextcloud = ${QUOTA}G"
if [[ ${SPACE_QUOTA} -ne "0" ]]; then PRINT+=" (Quota souscrit = ${SPACE_QUOTA}G)"; fi
PRINT+="\n         - Consommation réelle disque = ${GLOBAL}\n         ---\n \
        Nombre d'utilisateurs :\n \
        - Comptabilisés = ${USERS}"
if [[ ${USERS_QUOTA} -ne "0" ]]; then PRINT+=" (Quota souscrit = ${USERS_QUOTA})"; fi
PRINT+="\n         - Total réel = ${TOTALUSERS}, dont ${DISABLEDUSERS} désactivé(s) (et 1 Lydra)\n"
PRINT+="\n${EXPAND}\n"

log "${INFO}${PRINT}"
SPACE_QUOTA=$(( SPACE_QUOTA - 10 ))
USERS_QUOTA=$(( USERS_QUOTA - 1 ))
if [[ -n "${MAILS}" || ( ( ${SPACE_QUOTA} -ne "0" && ${QUOTA} -gt ${SPACE_QUOTA} ) 
                        || ( ${USERS_QUOTA} -ne "0" && ${USERS} -ge ${USERS_QUOTA} ) ) ]]; then
        log "${INFO}Sending mail to: ${MAILS}"
        echo -e "${PRINT}" | mail -a "Content-Type: text/plain; charset=UTF-8" \
                -s "[Quota] ${DOMAIN} - ${CURRENT_DATE}" "${MAILS:-support@lydra.fr}"
fi

log "${OK}Ending space usage"
