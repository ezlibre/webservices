#!/bin/bash

# Script to launch on Nextcloud server

function main() {
    readvars "$@"

    install_remove_apps

    init_borg

    cron_borg
}

function readvars() {

    while [[ $# -gt 0 ]]
    do
    key="$1"

    case $key in
        -d|--domain)
            if [ -w "$2" ]; then
                DOMAIN="${2}"
            else
                echo "[ERROR] Invalid domain : $2"
                exit 1
            fi
            shift
            ;;
        -h|--host)
            if [ -w "$2" ]; then
                SERVERNAME="${2}"
            else
                echo "[WARNING] No server name to configure backup : $2"
            fi
            shift
            ;;
        *)
            echo "[WARNING] Unkown option: $1"
            ;;
    esac
    shift
    done

	if [ -z "${DOMAIN}" ]; then
		echo '[ERROR] No valid domain.'
		exit 1
    else
        CLEANDOMAIN=$(echo ${DOMAIN} | sed 's,https*://,,g' | sed 's,/,,g' | sed 's,\.,,g' | sed 's,-,,g')
	fi

    return 0
}

function install_remove_apps(){
    sudo -u www-data php -f /data/cloud.${DOMAIN}/www/occ app:install calendar contacts tasks group_everyone circles spreed passwords
    sudo -u www-data php -f /data/cloud.${DOMAIN}/www/occ app:enable calendar contacts tasks group_everyone circles spreed passwords
    sudo -u www-data php -f /data/cloud.${DOMAIN}/www/occ app:disable nextcloud_announcements updatenotification survey_client
    echo "Reste à activer Collabora ou bien Community Document Server + ONLYOFFICE"
    echo "et à configurer..."
    echo "- Personnel > Informations personnelles : adresse e-mail = support@lydra.fr"
    echo "- Personnel > Vie Privée : emplacement du serveur = France"
    echo "- Administration > Paramètres de base : Serveur e-mail = <informations Tipimail avec expéditeur = ne-pas-repondre@lydra.fr>"
    echo "- Administration > Partage > Serveurs de confiance : cocher la case 'Ajouter un serveur automatiquement...'"
    echo "- Administration > Sécurité > Politique de mots de passe : cocher les 3 premières cases"
    echo "- Administration > Personnaliser l'apparence : mettre nom et URL du client"
    echo "- Administration > Travail collaboratif : décocher 'Générer automatiquement un agenda d'anniversaire'"
    echo "- Administration > Discussion > Serveur STUN : cloud.${DOMAIN}:3478"
    echo "- Administration > Discussion > Serveur TURN : cloud.${DOMAIN}:3478 + le secret dispo sur le serveur dans /etc/turnserver.conf"
}

function init_borg() {
    echo "Il faut générer une clé dans KeePass et la rentrer ici deux fois"
    borg init -e repokey ${CLEANDOMAIN}:/data/${CLEANDOMAIN}/borg/$(echo ${SERVERNAME} | sed 's+\..*++g')
    borg key export ${CLEANDOMAIN}:/data/${CLEANDOMAIN}/borg/$(echo ${SERVERNAME} | sed 's+\..*++g') /root/.borg/${CLEANDOMAIN}.key
    echo "Recopier la clé /root/.borg/${CLEANDOMAIN}.key dans un fichier à enregistrer dans Avancé > Pièces jointes de KeePass"
    echo "puis supprimer ce fichier .key de son ordinateur et du serveur"
}

function cron_borg() {
    echo "0  1  *  *  * root   /usr/local/bin/borg-backup_${CLEANDOMAIN}.sh" >> /etc/cron.d/borg
    cp /usr/local/bin/borg-backup_template.sh /usr/local/bin/borg-backup_${CLEANDOMAIN}.sh
    sed -i "3iCLEANDOMAIN=${CLEANDOMAIN}" /usr/local/bin/borg-backup_${CLEANDOMAIN}.sh
    sed -i "4iDOMAIN=${DOMAIN}" /usr/local/bin/borg-backup_${CLEANDOMAIN}.sh
    sed -i "5iSERVERNAME=$(echo ${SERVERNAME} | sed 's+\..*++g')" /usr/local/bin/borg-backup_${CLEANDOMAIN}.sh
    /usr/local/bin/borg-backup_${CLEANDOMAIN}.sh
}

main "$@"
