## 2.0.3
11/12/2018 - Database optimisation + PHP modules dependency

* New option to install (on Debian) missing PHP module `php-imagick`

## 2.0.2
10/12/2018 - Latest version check correction

* Resolve latest version getting number error
* Avoid upgrading from a major release to another other than major release + 1 (ex: Nextcloud 13 to 15)

## 2.0.1
08/12/2018 - INTEGRITY_CHECK and MISSING_FILES errors

* Disabling integrity check due to invalid checksum of `aws` app in zip and tar.gz Nextcloud 14 bundle

## 2.0.0
29/11/2018 - End of ownCloud support and others

* ownCloud end of support. Now only for Nextcloud
* `shellcheck` corrections
* Old versions (9.1.3) specificity removed
* `.htaccess` update bug corrected
* Database optimisation adding
* `data` directory now managed when not separated from Nextcloud root

## 1.0.3
26/09/2018 - Current version fetching

* Current version fetching patch

## 1.0.2
04/07/2017 - Nextcloud version control

* Version control updated for Nextcloud instances

## 1.0.1
28/02/2017 - Restore function correction

* Restore function correction

## 1.0.0
First use in production with ownCloud and Nextcloud instances.

* Adjustment of directory permissions
* Nextcloud adaptation
* ownCloud/Nextcloud version control
* Logging optimisation
* Backup and restore function enhancement

## 0.0.1
Initial script imported from https://git.karolak.fr/snippets/16

* Some changes to have parameters in variables