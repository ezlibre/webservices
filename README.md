# webservices

Scripts and other tools for various web services

## Nextcloud
Update a Nextcloud instance after backing up database and files (except "data" dir).

```
./nextcloud-upgrade.sh -d [NEXTCLOUD] -s [SAVE_DIR] -v [VERSION] --mods -l
	-d [NEXTCLOUD]	(mandatory)	/path/of/Nextcloud/instance
	-s [SAVE_DIR]		(optionnal)	/path/to/save/dir
	-v [VERSION]		(optionnal)	version to upgrade (ex: 14.0.4)
	--mods			(optionnal)	control and install of optionnal php modules (ex: imagick)
	-l			(optionnal)	verbose script

./nextcloud-upgrade.sh -s /backup/nextcloud -d /var/www/nextcloud -v 14.0.4 --mods -l
```